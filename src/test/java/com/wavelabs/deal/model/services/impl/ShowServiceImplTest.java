package com.wavelabs.deal.model.services.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.InvestmentRepository;
import com.wavelabs.deal.domain.repositories.ShowInterestRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.utilities.ObjectBuilder;

@RunWith(MockitoJUnitRunner.class)
public class ShowServiceImplTest {
	static final Logger log = Logger.getLogger(ShowServiceImplTest.class);
	@Mock
	ShowInterestRepository showInterestRepo;
	@Mock
	DealRepository dealRepo;
	@Mock
	UserRepository userRepo;
	@Mock
	InvestmentRepository investRepo;
	@InjectMocks
	ShowInterestServiceImpl showInterestServiceImpl;
	@Mock
	Investment invest;

	/**
	 * test_ShowInterest_On_Deal() method is for the people who shows interest
	 * on deal. If the given deal uuid doesn't exists, make a new entry in deal.
	 * 
	 */
	@Test
	public void test_ShowInterest_On_Deal() {
		//create Investment object 
		Investment invest = ObjectBuilder.getInvestment();
		Deal deal = ObjectBuilder.getDeal();
		//if the deal object is null, make a new entry in deal.
		when(dealRepo.findByUuid(anyString())).thenReturn(null);
		Deal deal1 = ObjectBuilder.getDeal1();
		deal1.setSecuredFunding(0.0);
		deal1.setUuid("ab12");
		showInterestServiceImpl.showInterestOnDeal("ab12", "abc123");
		when(dealRepo.save(any(Deal.class))).thenReturn(deal1);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void test_ShowInterest_On_Deal_Is_Null() {
		//create Investment object 
		Investment invest = ObjectBuilder.getInvestment();
		Deal deal = ObjectBuilder.getDeal();
		//if the deal object is null, make a new entry in deal.
		when(dealRepo.findByUuid(anyString())).thenReturn(deal);
		Deal deal1 = ObjectBuilder.getDeal1();
		deal1.setSecuredFunding(0.0);
		deal1.setUuid("ab12");
		showInterestServiceImpl.showInterestOnDeal("ab12", "abc123");
		when(dealRepo.save(any(Deal.class))).thenThrow(Exception.class);

	}
}