package com.wavelabs.deal.model.services.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.domain.model.user.User;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.InvestmentRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.utilities.ObjectBuilder;

@RunWith(MockitoJUnitRunner.class)

public class InvestmentServiceImplTest {
	@Mock
	InvestmentRepository investRepo;
	@Mock
	DealRepository dealRepo;
	@Mock
	UserRepository userRepo;
	@InjectMocks
	InvestmentServiceImpl investmentImpl;

	/**
	 * test_Process_Investment_Amount() is for adding Amount based on deals.
	 * Store the amount, dealId, userId on Investment object. Get the total
	 * amount by using dealId. Update the securedFunding with that total amount
	 * which is in Deal Object.
	 */
	@Test
	public void test_Process_Investment_Amount() {
		// create an object set the values what it needs
		Investment investment = ObjectBuilder.getInvestment();
		Deal deal = ObjectBuilder.getDeal();
		User user = ObjectBuilder.getUser();
		when(dealRepo.findByUuid(anyString())).thenReturn(deal);
		when(userRepo.findByUuid(anyString())).thenReturn(user);
		investment.setDeal(deal);
		investment.setUser(user);
		investment.setAmount(1000.0);
		// mock the db call
		when(investRepo.save(any(Investment.class))).thenReturn(investment);
		when(dealRepo.findSecuredFundingById(anyString())).thenReturn(1000.0);
		// when calling a method, checks the actual implementation class
		investmentImpl.processInvestAmount("abc123", "ab123", 1000.0);
		// checks whether the update executes at least once.
		verify(dealRepo, times(1)).updateSecuredFundingByUuid(1000.0, "abc123");

	}

	/**
	 * test_Process_Investment_Amount_Is_Null() method return null when the
	 * exception occurs while instantiating object.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void test_Process_Investment_Amount_Is_Null() {
		/*// create an object set the values what it needs
		Investment investment = ObjectBuilder.getInvestment();
		Deal deal = ObjectBuilder.getDeal();
		User user = ObjectBuilder.getUser();
		when(dealRepo.findByUuid(anyString())).thenReturn(deal);
		when(userRepo.findByUuid(anyString())).thenReturn(user);
		investment.setUser(user);
		investment.setDeal(deal);
		investment.setAmount(10000.0);
		// mock the db call, object return null, throws an exception.
		when(investRepo.save(any(Investment.class))).thenThrow(Exception.class);
		when(dealRepo.findSecuredFundingById(anyString())).thenReturn(1000.0);
		// when calling a method, checks the actual implementation class
		investmentImpl.processInvestAmount("abc123", "ab123", 1000.0);*/

	}
}
