package com.wavelabs.login.model.responses;

public class LoginSucess {
	private String code;
	private int state;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

}
