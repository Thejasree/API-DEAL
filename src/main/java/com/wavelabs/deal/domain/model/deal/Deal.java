package com.wavelabs.deal.domain.model.deal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 
 * @author thejasreem Deal is a Pojo class parameters: id and FundingStatus It
 *         has setters and getters
 */
@Entity
public class Deal {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String uuid;
	private Double securedFunding;
	private String name;

	public Deal() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Double getSecuredFunding() {
		return securedFunding;
	}

	public void setSecuredFunding(Double securedFunding) {
		this.securedFunding = securedFunding;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
