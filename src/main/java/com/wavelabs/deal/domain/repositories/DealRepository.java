package com.wavelabs.deal.domain.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.wavelabs.deal.domain.model.deal.Deal;

/**
 * 
 * @author thejasreem DealRepository is used for persistence.
 */
public interface DealRepository extends JpaRepository<Deal, Integer> {
	public Deal findByUuid(String uuid);
	
	/*@Query("select u.name as username,sum(i.amount) as investedAmount,u.image as image from User u, Investment i, Deal d where d.uuid=:dealUuid and d.id=i.deal and u.id=i.user group by u.name")
	HashSet<PeopleInterest> findPeopleInterestedByUuid(@Param("dealUuid") String dealUuid);*/
	
	@Modifying
	@Transactional
	@Query("update Deal d set securedFunding=:dealAmount where d.uuid=:dealUuid")
	void updateSecuredFundingByUuid(@Param("dealAmount") Double dealAmount, @Param("dealUuid") String dealUuid);	
	
	@Query("select sum(i.amount) from Investment i,Deal d where d.uuid=:dealUuid and i.deal=d.id")
	Double findSecuredFundingById(@Param("dealUuid")String dealUuid);

	
	
	
}
