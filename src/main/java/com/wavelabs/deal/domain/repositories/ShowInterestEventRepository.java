package com.wavelabs.deal.domain.repositories;


import org.springframework.data.repository.CrudRepository;

import com.wavelabs.deal.model.events.ShowInterestHandler;

/**
 * 
 * @author thejasreem ShowInterestEventRepository() for event persistence.
 */
public interface ShowInterestEventRepository extends CrudRepository<ShowInterestHandler, Integer> {

}