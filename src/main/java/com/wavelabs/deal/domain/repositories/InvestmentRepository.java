package com.wavelabs.deal.domain.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.investment.Investment;


public interface InvestmentRepository  extends JpaRepository<Investment, Integer> {


	
	
	public Investment[] findByDeal(Deal deal);
	

}
