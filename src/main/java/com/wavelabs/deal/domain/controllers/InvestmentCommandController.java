package com.wavelabs.deal.domain.controllers;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.model.service.InvestmentService;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * 
 * @author thejasreem InvestmentCommandController is the controller class for
 *         Investment class. createNewInvest() method is used for processing
 *         investment amount.
 * 
 */
@RestController
@Component
public class InvestmentCommandController {
	@Autowired
	InvestmentService investService;
	@Autowired
	RestMessage restMessage;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/deal/{dealUuid}/investment/{userUuid}/{amount}", method = RequestMethod.POST)
	public ResponseEntity processInvestedAmount(@RequestParam String userUuid, @RequestParam String dealUuid,
			@RequestParam double amount) {
		Investment invest = investService.processInvestAmount(dealUuid, userUuid, amount);
		if (invest != null) {
			restMessage.messageCode = "200";
			restMessage.message = "Investment  successfull!";
			return ResponseEntity.status(200).body(restMessage);
		} else {
			restMessage.messageCode = "404";
			restMessage.message = "Investment unsuccessfull!";
			return ResponseEntity.status(404).body(restMessage);
		}

	}

}
