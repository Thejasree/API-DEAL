package com.wavelabs.deal.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.wavelabs.deal.model.events.ShowInterestHandler;

import io.nbos.capi.api.v0.models.RestMessage;
/**
 * 
 * @author thejasreem
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan({ "com.wavelabs.deal", "com.wavelabs.deal.model.services.impl", "com.wavelabs.deal.model.service",
		"com.wavelabs.deal.model.utility","com.wavelabs.deal.model.event.service"})
@EnableJpaRepositories("com.wavelabs.deal.domain.repositories")
@EntityScan({ "com.wavelabs.deal.domain.model","com.wavelabs.deal.model.events"})
public class DealServiceApplication {

	/**
	 * Method that Spring Boot detects when you run the executable jar file.
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(DealServiceApplication.class, args);

	}

	@Value("${dealUrl}")
	private String dealUrl;
	@Value("${dealIdUrl}")
	private String dealIdUrl;
	

	@Bean
	RestMessage restMessage() {
		return new RestMessage();
	}
	@Bean
	ShowInterestHandler showInteresthandler(){
		return new ShowInterestHandler();
				
	}
}
